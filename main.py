#!/usr/bin/env python

import sys
import time
import warnings
import numpy as np
from datetime import timedelta
import matplotlib.pyplot as plt
warnings.filterwarnings("ignore")

from util import *
from opts import *
from plot import *
from parse import *
from preproc import downsampling

from sklearn.externals import joblib
from sklearn.model_selection import LeaveOneOut
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, f1_score


def main(Opts):

    # Set output stream.
    if Opts['OutFile'] is not None:
        sys.stdout = open(Opts['OutFile'], "wb")

    # Print Options
    if Opts['Verb'] > 0:
        print_dict(Opts)
        sys.stdout.flush()

    start = time.time()

    # Parse driver data
    driver_data = []
    for driver in Opts['Driver']:

        data = parse_driver(driver,
                            Opts['Theta'],
                            Opts['Rate'],
                            Opts['WinSize'],
                            Opts['KalQ'],
                            Opts['KalR'])

        driver_data.append(data)

    # Init RFC
    clf = RandomForestClassifier(n_estimators=Opts['NumClass'],
                                 criterion='entropy',
                                 n_jobs=-1)

    # LODO
    n = range(len(driver_data))
    loo = LeaveOneOut()
    feats = []
    stds = []
    CM = []
    F1 = []
    clfs = []
    for tr_idx, ts_idx in loo.split(n):
        tr_idx = tr_idx.tolist()
        ts_idx = ts_idx.tolist()

        # Select and stack
        train_data = np.vstack([ driver_data[i] for i in tr_idx ])
        test_data = np.vstack([ driver_data[i] for i in ts_idx ])

        # Downsample and shuffle data
        train_data = downsampling(train_data)
        test_data = downsampling(test_data)

        # Separate feats and labels
        Xtr = train_data[:,:-1]
        ytr = train_data[:,-1]
        Xts = test_data[:,:-1]
        yts = test_data[:,-1]

        # Train
        clf.fit(Xtr, ytr)

        # Feature Importance
        featsi = [ x.feature_importances_ for x in clf.estimators_ ]
        featsi = np.array(featsi)
        stdi = np.std(featsi, axis=0)
        featsi = np.mean(featsi, axis=0)

        # Predict
        y_hat = clf.predict(Xts)

        # Metrics
        CMi = confusion_matrix(yts, y_hat, labels=[0, 1, 2, 3])
        F1i = f1_score(yts, y_hat, average='weighted')

        feats.append(featsi)
        stds.append(stdi)
        clfs.append(clf)
        CM.append(CMi)
        F1.append(F1i)

    # Stop timer
    elapsed = time.time() - start
    timestr = str(timedelta(seconds=elapsed))

    # Average results
    feats = np.array(feats).mean(axis=0)
    stds = np.array(stds).mean(axis=0)
    CM = np.array(CM).mean(axis=0)
    F1 = np.array(F1).mean(axis=0)

    # Save files for postproc
    if Opts['OutDir'] is not None:

        try:
            os.makedirs(Opts['OutDir'])
        except OSError:
            pass

        # data files
        fname = os.path.join(Opts['OutDir'], "fi.txt")
        np.savetxt(fname, np.c_[ feats, stds ], fmt="%16.6e")
        fname = os.path.join(Opts['OutDir'], "cm.txt")
        np.savetxt(fname, CM, fmt="%16.6e")

        # classifiers
        # this is correct only when we pass all 6 drivers
        # otherwise I should extract the driver index from the
        # drivers list, but I am too lazy to do it
        for i, clf in enumerate(clfs, start=1):
            fname = os.path.join(Opts['OutDir'], "forest_testD%d.pkl" % i)
            with open(fname, "wb") as f:
                joblib.dump(clf, f)


    # Print summary
    print("# Average weighted F1-score: %.4f" % F1)
    print("# Total execution time     : %s" % timestr)

    # Labels for plots
    classes = [ "No Event", "Braking", "Turning", "Accel." ]
    featnames = [ "v", "ax", "ay", "az", "gx", "gy", "gz", "roll", "pitch",
                  "yaw" ]
    statnames = [ "mean", "var", "std", "min", "max", "rrange", "iqr", "skew",
                  "kurtosis", "slope" ]

    # Plots
    plot_cm(CM, classes, title='Confusion Matrix')
    if Opts['Save']:
        plt.savefig("cm.png", dpi=300)

    plot_fi(feats, featnames, statnames, title='Features Importance')
    if Opts['Save']:
        plt.savefig("fi.png", dpi=300)

    plot_fi(stds, featnames, statnames, title='Std. Deviation of FI')
    if Opts['Save']:
        plt.savefig("fi_sigma.png", dpi=300)

    if Opts['Show']:
        plt.show()

    return


if __name__ == '__main__':

    Opts = options()
    main(Opts)

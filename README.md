Manual
======
------------------------------------------------------------------------

Requirements
============

The code has been tested on the following OSs:

-   Ubuntu 16.04
-   Fedora 28
-   Windows 10

It can run with the following versions of Python:

-   Python 2.7.15
-   Python 3.6.4

It requires the following packages (in parenthesis the version used):

-   NumPy (1.14.1, 1.15.1)
-   SciPy (1.0.0, 1.0.1)
-   Matplotlib (2.1.2, 2.2.2)
-   Pandas (0.22.0, 0.23.4)
-   Scikit-learn (0.19.1, 0.19.2)

Usage
=====

The file to be executed is `main.py`. Executing it will use default
options( _i.e._ the ones described in 1), that can be seen in the help by
running

      $ python main.py -h

Execution results in a printout to standard output with the average
weighted _F1_-score (~75%) and execution time (~1 min). The help lists all the
possible options the script accepts.

-   `-p, --path` : this option specifies the root folder of the data set.

-   `-d, --driver` : this option specifies the subfolders containing driver
    data to be included in the analysis.

-   `-w, --window` : this option specifies the width of the time windows
    (in s) for the analysis of the time series.

-   `-r, --rate` : this option specifies the sampling rate (in Hz) for the
    resampling of the time series.

-   `-t, --theta` : this option specifies the fraction of the time windows
    to be considered for label generation.

-   `-kq, --kq` : this option specifies the _Q_ parameter for the
    Kalman filter.

-   `-kr, --kr` : this option specifies the _R_ parameter for the
    Kalman filter.

-   `-nc, --nc` : this option specifies the number of Decision Trees in the
    Random Forest.

-   `-o, --output` : this option specifies a file for logging, in
    alternative to standard output.

-   `--outdir` : this option specifies an output directory to save files
    for postprocessing.

-   `--show` : this option opens a window to show the confusion matrix.

-   `--save` : this option allows to save a png file for the
    confusion matrix.

-   `-v, --verbosity` : this option increases the verbosity of the output.

The `run_tests.csh` script runs the `main.py` script with various options to
scan for hyperparameters.

References
==========
1. J. Xie, A. R. Hilal and D. Kuli, _IEEE Sensors Journal_, 2018, **18**, 4777-4784.

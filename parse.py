#!/usr/bin/env python

import os
import numpy as np
import pandas as pd

from feats import *
from preproc import *


def parse_driver(driver, theta=0.8, rate=10.0, wintime=3.4, Q=1e-4, R=1e-2):

    # List dirs in driver dir.
    dirs = os.listdir(driver)
    dirs = [ x for x in dirs if "MOTORWAY" in x ]

    # Features data
    fnames = [ "RAW_GPS.txt", "RAW_ACCELEROMETERS.txt" ]
    cols = [ [0, 1], [0, 1, 2, 3, 4, 8, 9, 10] ]
    names = [ ["t", "v"], ["t", "act", "ax", "ay", "az", "gx", "gy", "gz"] ]

    # Labels data
    lfname = "EVENTS_INERTIAL.txt"
    lcols = [0, 1]
    lnames = ["t", "type"]

    # Loop over directories
    for idir in dirs:

        data = []
        for i, fname in enumerate(fnames):

            # Make filename
            fname = os.path.join(driver, idir, fname)

            # Read data with column names
            df = pd.read_csv(fname, sep=" ", header=None, usecols=cols[i],
                             names=names[i])

            # Set index to time
            df.index = pd.to_datetime(df["t"], unit="s")

            # Remove useless column (now index)
            df = df[names[i][1:]]

            # Filter accelerometer when active
            if "ACCEL" in fname:
                df = df[df["act"] == 1]
                df = df[names[i][2:]]

            # Before saving and data processing, convert to consistent
            # units.
            if "GPS" in fname:
                # GPS: v in km / h -> m / s
                df["v"] = df["v"] * 1000.0 / 3600.0

            if "ACCEL" in fname:
                # ACCEL: a in Gs -> m / s**2
                df["ax"] = df["ax"] * 9.81
                df["ay"] = df["ay"] * 9.81
                df["az"] = df["az"] * 9.81

                # ACCEL: gyro in deg -> rad
                df["gx"] = df["gx"] * np.pi / 180.0
                df["gy"] = df["gy"] * np.pi / 180.0
                df["gz"] = df["gz"] * np.pi / 180.0

            # # Save before data proc. This is basically useless, just for
            # # analysis
            # save_data(fname, df)

            # Here resampling and interpolation for numerical data
            df = df_resample(df, rate)

            # # Save after resampling
            # save_data(fname, df, suffix="INT")

            # Here Kalman filter for numerical data
            df = df.apply(kalman, axis=0, args=(Q, R))

            # # Save after filtering
            # save_data(fname, df, suffix="KAL")

            # Compute missing features. Here not really sure what to do:
            # Xie claims they use Roll, Pitch and Yaw from Romero, however
            # units are inconsistent with what reported by Romero
            # (rad / s in Xie's paper, deg in Romero's).
            # Xie uses also gyroscopic data that are consistent with
            # Romero's units.
            # We assume Xie's gyroscopic data are Romero's data, and
            # we compute the rad / s Roll, Pitch, and Yaw deriving
            # over time.
            if "ACCEL" in fname:
                roll = pd.Series(np.gradient(df['gx'].values), df.index,
                                 name='roll')
                pitch = pd.Series(np.gradient(df['gy'].values), df.index,
                                  name='pitch')
                yaw = pd.Series(np.gradient(df['gz'].values), df.index,
                                name='yaw')
                df = pd.concat([df, roll, pitch, yaw], axis=1)

            data.append(df)

        # Here we parsed all the features in idir
        # data[0] contains the df with GPS data
        # data[1] contains the df with ACCEL data
        # join them only where we have data from both GPS and ACCEL
        df = pd.merge(data[0], data[1], how="inner",
                      left_index=True, right_index=True)

        # Get labels
        lname = os.path.join(driver, idir, lfname)
        ldf = pd.read_csv(lname, sep=" ", header=None, usecols=lcols,
                          names=lnames)

        # Set index to time
        ldf.index = pd.to_datetime(ldf["t"], unit="s")

        # Remove useless column (now index)
        ldf = ldf[lnames[1:]]

        # Here resampling and interpolation for numerical data
        ldf = df_resample(ldf, rate, interpolate=False)

        # Join numerical data with labels
        df = pd.merge(df, ldf, how="inner", left_index=True, right_index=True)

        # Here windowing
        dfs = df_split(df, rate, wintime)
        dfs_feats = [ x.iloc[:,:-1] for x in dfs ]
        dfs_labels = [ x.iloc[:,-1] for x in dfs ]

        # Get features
        feats = stat_windows(dfs_feats)

        # Here label generation for windows
        labels = mk_labels(dfs_labels, theta, wintime)

        # Merge Feats and labels for a more comfortable processing
        ftslbls = np.c_[ feats, labels ]

        # Stack data from this dir to previous dirs
        try:
            total = np.r_[ total, ftslbls ]
        except:
            total = ftslbls

    return total


def save_data(fname, df, suffix=None):

    folder = "analysis"
    try:
        os.makedirs(folder)
    except OSError:
        pass

    name = os.path.normpath(fname)
    name = name.split(os.sep)
    name0 = name[1]
    name1 = name[-2].split("-")[-1]
    name2 = name[-1].split(".")[0]

    if suffix:
        name2 = name2 + "_%s" % suffix

    name = name0 + "_" + name1 + "_" + name2 + ".csv"
    name = os.path.join(folder, name)
    with open(name, "wb") as f:
        df.to_csv(f)

    return


if __name__ == '__main__':

    import sys
    df = parse_driver(sys.argv[1])
    pass

#!/usr/bin/env python

import sys
import numpy as np
import scipy.stats

thismodule = sys.modules[__name__]


def stat_windows(df_list):

    for df in df_list:
        stat = stat_feats(df)

        try:
            total = np.r_[ total, stat ]
        except:
            total = stat

    return total


def stat_feats(df):

    # Statistical Features, Sec. III.B.1 of Xie's paper
    # df comes in with the following cols
    # v ax ay az gx gy gz roll pitch yaw

    # Stat feats
    # All built-in, except range, slope, jerk
    feats = [ "mean", "var", "std", "min", "max", "rrange", "iqr", "skew",
              "kurtosis", "slope", "jerk" ]

    stats = []
    for feat in feats:

        # Look first in numpy, then in scipy, finally in current module
        try:
            func = getattr(np, feat)
        except AttributeError:
            try:
                func = getattr(scipy.stats, feat)
            except AttributeError:
                func = getattr(thismodule, feat)

        data = func(df, axis=0)
        stats.extend(data)

    stats = np.array(stats).reshape(1,-1)

    return stats


def rrange(x, axis=None):
    return np.max(x, axis=axis) - np.min(x, axis=axis)


def slope(x, axis=None):

    dt = (max(x.index) - min(x.index)).total_seconds()
    if dt == 0:
        return np.zeros(x.shape[1])

    dx = rrange(x, axis=axis)

    return dx / dt


def jerk(x, axis=None):

    dt = (max(x.index) - min(x.index)).total_seconds()
    if dt == 0:
        return [0]

    acc = [ "ax", "ay", "az" ]
    x = x[acc].iloc[[0,-1]]
    da = np.linalg.norm(x, axis=1, keepdims=True)
    da = np.diff(da, axis=axis)

    return da / dt


if __name__ == '__main__':
    pass

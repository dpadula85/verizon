%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simple Sectioned Essay Template
% LaTeX Template
%
% This template has been downloaded from:
% http://www.latextemplates.com
%
% Note:
% The \lipsum[#] commands throughout this template generate dummy text
% to fill the template out. These commands should all be removed when 
% writing essay content.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-------------------------------------------------------------------------------

%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%-------------------------------------------------------------------------------


\documentclass[11pt]{article}

\usepackage[scale=0.8]{geometry}
\geometry{a4paper}

\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{float} % Allows putting an [H] in \begin{figure} to specify the exact location of the figure
\usepackage{siunitx}
\usepackage{physics}
\usepackage{hyperref}
\usepackage{multicol}
\usepackage[inline]{enumitem}
\usepackage{rsc}

\hypersetup{colorlinks}
% \hypersetup{colorlinks,citecolor=black,filecolor=black,linkcolor=black,urlcolor=black}

\linespread{1.5} % Line spacing
\setlength\parindent{0pt} % Uncomment to remove all indentation from paragraphs

\renewcommand*{\thefootnote}{\alph{footnote}}

\begin{document}

%-------------------------------------------------------------------------------
%	TITLE PAGE
%-------------------------------------------------------------------------------
\begin{titlepage}

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}} % Defines a new command for the horizontal lines, change thickness here
\center

\HRule \\[0.4cm]
{ \huge \bfseries Report}\\ % Title
\HRule \\[1.5cm]

{\large \today}\\[3cm]

\vfill

\end{titlepage}

%-------------------------------------------------------------------------------
%	TABLE OF CONTENTS
%-------------------------------------------------------------------------------
\tableofcontents
\newpage

%-------------------------------------------------------------------------------
%	BODY
%-------------------------------------------------------------------------------
\section{Objective}
The objective of this study is to reproduce part of the analyses reported by 
Xie\cite{Xie} on the data set by Romera\cite{Romera}. The task can be 
formalised as a classification of driver maneuvers to be carried out with a 
machine learning algorithm.

\section{Data Processing}

\subsection{Parsing, Additional Features and Unit Conversion}
The code provided parses the information required from the files provided in 
Romera's data set.\cite{Romera} As reported by Xie,\cite{Xie} only highway 
driving session are processed. Currently, the filtering of data to process is 
hardcoded in the parsing function (module \verb|parse.py|), but it is possible 
to add it as an option.

The data gathered by Romera and used in the analyses are physical features from 
a smartphone GPS and accelerometer, and are the following:
\begin{multicols}{2}
 \begin{enumerate}
  \item $v$: velocity (\si{\km\per\hour})
  \item $a_x$: acceleration along $x$ (\si{\metre\per\square\second})
  \item $a_y$: acceleration along $y$ (\si{\metre\per\square\second})
  \item $a_z$: acceleration along $z$ (\si{\metre\per\square\second})\break
  \item $g_x$: gyro along $x$ (\si{\deg})
  \item $g_y$: gyro along $y$ (\si{\deg})
  \item $g_z$: gyro along $z$ (\si{\deg})
  \item[]
 \end{enumerate}
\end{multicols}

Here there is a first discrepancy in terms of nomenclature between the two 
papers. In short, Romera provides data under the name of Roll, Pitch, Yaw with 
the units of \si{\deg}. Xie, instead, has a different unit for features called 
Roll, Pitch, Yaw (\si{\radian\per\second}), but the unit of \si{\radian} for 
features called $g_x$, $g_y$, $g_z$. In the following, we will assume that 
Romera's Roll, Pitch, and Yaw, correspond to Xie's $g_x$, $g_y$, $g_z$, because 
of unit consistency. The additional features required by Xie are the Roll 
($r$), Pitch ($p$), and Yaw ($y$) expressed in \si{\radian\per\second}, which 
can be obtained by deriving $g_x$, $g_y$, $g_z$, respectively, over time, as in
\begin{multicols}{3}
 \begin{equation}
  r = \dv{g_x}{t}
 \end{equation}\break
 \begin{equation}
  p = \dv{g_y}{t}
 \end{equation}\break
 \begin{equation}
  y = \dv{g_z}{t}
 \end{equation}
\end{multicols}

The derivation operation has been decided based on units and on the fact that 
these quantities are angular analogues of linear quantities. I will actually 
use Xie's nomenclature, but I think it is wrong, as Roll, Pitch and Yaw should 
be angles, and gyroscope data angular velocities, thus Xie is using either 
wrong names or units.
These features are actually computed after the upsampling and filtering 
described in Section~\ref{Sec:Up&Fil}, but they are mentioned now for 
consistency of exposition.

The final set of physical features includes thus 10 quantities. Notice that Xie 
uses features with inconsistent units (\textit{e.g.} $v$ and $a_x$). In the 
preprocessing, I decided to convert all of the features to consistent units (SI 
units for speed and acceleration, radians for angles). Both features and labels 
have been treated according to the procedures described in the next sections.

\subsection{Upsampling and Filtering}
\label{Sec:Up&Fil}
For an easier data processing and to increase the quantity of data, Xie reports 
an upsampling procedure, which consists in generating new timestamps for the 
data set at the desired rate (\SI{10}{\per\second}) that can be set by the 
user. The missing data at the new timestamps are then obtained by interpolating 
the data provided by Romera.
In our case, the interpolation has been carried out only for features, while 
labels have not been interpolated, as they describe separate classes and 
interpolation would be meaningless.

Finally, to reduce the noise in the interpolated data, the time series has been 
filtered with a Kalman filter. Also in this case, we filtered only features. 
For the Kalman filter, we assumed independent variables. The filter can be 
regulated by two parameters, representing the variance between measurements, 
\textit{i.e.} how smooth the resulting function would be.

The features come from two different files in Romera's data set, and the labels 
from a third file. Before moving to the windowing process, the data are joined 
in a unique data set, losing incomplete information.

\subsection{Windowing}
The data set generated in Section~\ref{Sec:Up&Fil} is then split into time 
windows of equal length $w$ that can be set by the user. For each of these 
windows, for each physical feature we compute a set of statistical features, 
namely
\begin{enumerate*}[label=\itshape\roman*\upshape)]
 \item mean,
 \item variance,
 \item standard deviation,
 \item minimum,
 \item maximum,
 \item range,
 \item interquartile range,
 \item skewness,
 \item kurtosis,
 \item slope,
\end{enumerate*}
Additionally, the \textit{jerk} (\textit{i.e.} $\dv{\norm{a}}{t}$) of the 
window is calculated. This means that each window will be described by a total 
of 101 features.

Each window is also assigned a label, according to Algorithm 1 in Xie's 
paper.\cite{Xie} Here I haven't understood the algorithm very well, I tried to 
implement something similar according to my understanding. What I implemented 
works as follows. We need an arbitrary parameter $0 \leq \theta \leq 1$ to 
set the fraction of the window we want to consider to generate the label.

\begin{enumerate}
 \item set a No Event label $l_{prev} = 0$
 \item if the current window $w_i$ is longer than $w \cdot \theta$
 \begin{enumerate}[label*=\arabic*.]
  \item split the window in two fractions, the starting one, from $w_i^s$ to 
$w_i^s + w \cdot \theta$ , and the ending one from $w_i^s + w \cdot \theta$ to 
$w_i^e$
 \end{enumerate}
 \item count the labels falling in the starting window fraction. If no label is 
present assign $l_i = l_{prev}$, otherwise assign $l_i$ the mode of the labels 
falling in this fraction
 \item count the labels falling in the ending window fraction. If no label is 
present assign $l_{prev} = 0$, otherwise assign $l_{prev}$ the mode of the 
labels falling in this fraction
\end{enumerate}

\subsection{Cross-Validation and Downsampling}
The data is now ready to be fed to a Machine Learning algorithm. The 
Cross-Validation scheme described by Xie is a Leave-One-Driver-Out scheme. 
After gathering and preprocessing the data for each driver, a training and a 
test set are generated by merging the data. Before feeding it to the Random 
Forest Classifier, we carry out a downsampling of the majority class. 
Basically, the data set is strongly unbalanced towards one class, which has 
many more occurrences in the data set with respect to other classes. Elements 
of the data set belonging to the majority class are thus randomly discarded 
until we obtained a number of examples for this class in line with the average 
of number of examples for other classes.

\section{Results}
The results reported in this section have been obtained by running the 
code using the parameters described in Xie's paper.\cite{Xie} Since there are 
many adjustable parameters, the outcome of the model can be tuned by choosing 
the most appropriate ones (see Section~\ref{Sec:Hyperparams}).

The performance of the algorithm is evaluated by computing the average 
Confusion Matrix and weighted \textit{F1-score} for the Cross-Validation splits.
The average weighted \textit{F1-score} obtained for the run reported is 
$74.31\%$. The average confusion matrix is reported in Fig.~\ref{fig:cm}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\columnwidth]{./figs/cm.eps}
    \caption{Average Confusion Matrix obtained with Xie's parameters.\cite{Xie}}
    \label{fig:cm}
\end{figure}

The confusion matrix shows how the ``Turning'' maneuver is the main source of 
error, as it gets often misclassified as a ``No Event'' maneuver.

Random Forest algorithms allow to quantify features importance. In 
Fig.~\ref{fig:imp} I report the importance of physical and 
statistical features obtained with Xie's parameters. Notice that the 
\textit{jerk} feature is missing from this representation. 

\begin{figure}[H]
    \centering
        \begin{subfigure}[t]{0.4\columnwidth}

\raisebox{-\height}{\includegraphics[width=\textwidth]{./figs/fi_imp.eps}}
        \caption{Features importance.}
        \label{fig:fi}
    \end{subfigure}
    \hfill
    \begin{subfigure}[t]{0.4\columnwidth}

\raisebox{-\height}{\includegraphics[width=\textwidth]{./figs/fi_imp_std.eps}}
        \caption{Standard deviation of features importance.}
        \label{fig:fi_std}
    \end{subfigure}
    \caption{}
    \label{fig:imp}
\end{figure}

Concerning physical features, it appears that $v$, $a_z$, and $g_x$ play the 
most important role, in particular their minimum values. Concerning statistical 
features, it appears that minimum and standard deviation are the most important 
ones. Important features also show the highest standard deviation in the 
importance values.

\section{Hyperparameters Search}
\label{Sec:Hyperparams}
As mentioned previously, Xie's model has many tuneable hyperparameters, and 
their choices were not thoroughly discussed in the paper. A less arbitrary way 
of operating would be to scan for the best combinations of hyperparameters. I 
provide a script that scans some of these in a separate way, thus it neglects 
interactions among hyperparameters. An alternative would be to do a grid 
search, but since the number of hyperparameters is high, the computational cost 
of scanning a higher dimensional grid can rapidly increase. Possibly, the best 
solution would be to optimise them stochastically.

\section{Potential Improvements}
Here I provide a list of potential improvements to the implemented model.

\begin{enumerate}
 \item Better implementation of Kalman Filter, including laws of motion
 \item Hyperparameters search by grid scan or stochastic optimisation 
(\textit{e.g.} Differential Evolution)
\end{enumerate}

\bibliographystyle{rsc}
\bibliography{refs}

\end{document}

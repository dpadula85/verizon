#!/usr/bin/env python

import itertools
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# from http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html
def plot_cm(cm, classes,
            normalize=False,
            title='Confusion matrix',
            cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    plt.figure()

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

    return


def plot_fi(importances, featnames, statnames,
            title='Features importance',
            cmap=plt.cm.Blues):

    plt.figure()
    plt.title(title)

    # axis 0 statnames
    # axis 1 featnames
    n = len(statnames)
    m = len(featnames)

    # Exclude jerk
    imp = importances[:-1].reshape(n,m)

    plt.imshow(imp, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(n)
    tick_marks1 = np.arange(m)
    plt.xticks(tick_marks, statnames, rotation=45)
    plt.yticks(tick_marks1, featnames)

    plt.tight_layout()
    plt.xlabel('Statistical Feature')
    plt.ylabel('Physical Feature')

    return


if __name__ == '__main__':

    import sys
    raw = sys.argv[1]
    proc = sys.argv[2]

    df = pd.read_csv(raw, parse_dates=[0], sep=",", index_col="t")
    df1 = pd.read_csv(proc, parse_dates=[0], sep=",", index_col="t")

    tr = df.index
    dr = df["v"]

    tp = df1.index
    dp = df1["v"]

    # print np.sqrt(np.mean( (dr - dp)**2))
    plt.plot_date(tr, dr, color="b")
    plt.plot_date(tp, dp, color="orange", marker="x")
    plt.show()

#!/bin/tcsh

mkdir window
mkdir kalman

# Scan Window Size
set sizes = `seq 0.4 0.6 6`

foreach size ( $sizes )

    ./main.py -w $size -o win_${size}s.txt --outdir win_${size}s
    mv win_${size}s.txt win_${size}s

end
mv win_*s window

# Scan theta
set thetas = `seq 0.60 0.05 0.90`

foreach theta ( $thetas )

    ./main.py -t ${theta} -o theta_${theta}.txt --outdir theta_${theta}
    mv theta_${theta}.txt theta_${theta}

end
mv theta_* window

# Scan Q
set Qs = ( 0.1 0.01 0.001 0.0001 )

foreach Q ( $Qs )

    ./main.py -kq ${Q} -o KalQ_${Q}.txt --outdir KalQ_${Q}
    mv KalQ_${Q}.txt KalQ_${Q}

end
mv KalQ_* kalman

# Scan R
set Rs = ( 0.1 0.01 0.001 0.0001 )

foreach R ( $Rs )

    ./main.py -kr ${R} -o KalR_${R}.txt --outdir KalR_${R}
    mv KalR_${R}.txt KalR_${R}

end
mv KalR_* kalman

#!/usr/bin/env python

import os
import argparse as arg


def options():
    '''Defines the options of the script.'''

    parser = arg.ArgumentParser(
                formatter_class=arg.ArgumentDefaultsHelpFormatter)

    #
    # Input Options
    #
    inp = parser.add_argument_group("Input Data")

    inp.add_argument('-p', '--path', type=str, dest='Path',
                     default="UAH-DRIVESET-v1", help='''Data Set Path''')

    inp.add_argument('-d', '--driver', type=str, dest='Driver',
                     nargs='+', default=["D1", "D2", "D3", "D4", "D5", "D6"],
                     help='''Drivers to be included in the analysis.''')

    #
    # Data Processing Options
    #
    dat = parser.add_argument_group("Data Processing Options")

    dat.add_argument('-w', '--window', default=3.4, type=float, dest='WinSize',
                     help='''Window Size (s).''')

    dat.add_argument('-r', '--rate', default=10.0, type=float, dest='Rate',
                     help='''Sampling Rate (Hz).''')

    dat.add_argument('-t', '--theta', default=0.8, type=float, dest='Theta',
                     help='''Fraction of the window to be considered for label
                     generation.''')

    dat.add_argument('-kq', '--kq', default=1e-4, type=float, dest='KalQ',
                     help='''Kalman Filter Q variance.''')

    dat.add_argument('-kr', '--kr', default=1e-2, type=float, dest='KalR',
                     help='''Kalman Filter R variance.''')

    #
    # Calculations Options
    #
    calc = parser.add_argument_group("Calculation Options")

    calc.add_argument('-nc', '--nc', default=150, type=int, dest='NumClass',
                      help='''Number of Random Forest classifiers.''')

    #
    # Output Options
    #
    out = parser.add_argument_group("Output Options")

    out.add_argument('-o', '--output', default=None, type=str, dest='OutFile',
                     help='''Output File.''')

    out.add_argument('--outdir', default=None, type=str, dest='OutDir',
                     help='''Directory to save data for postprocessing.''')

    out.add_argument('--show', default=False, action='store_true', dest='Show',
                     help='''Show Confusion Matrix.''')

    out.add_argument('--save', default=False, action='store_true', dest='Save',
                     help='''Save Confusion Matrix Figure.''')

    out.add_argument('-v', '--verbosity', default=0, action="count",
                     dest="Verb", help='''Verbosity level''')

    args = parser.parse_args()
    Opts = vars(args)

    # Preprocess paths
    Opts['Driver'] = [ os.path.join(Opts['Path'], x)
                       for x in sorted(Opts['Driver']) ]

    return Opts


if __name__ == '__main__':
    pass

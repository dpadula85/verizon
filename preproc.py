#!/usr/bin/env python

import datetime
import numpy as np
from sklearn.utils import resample


def df_resample(df, rate, interpolate=True):

    # rate in Hz
    timestep = 1 / rate
    resample_str = "%fS" % timestep
    df = df.resample(resample_str).mean()

    if interpolate:
        df = df.interpolate(method="linear", axis=0)

    return df


def df_split(df, rate, wintime):

    # rate in Hz
    timestep = 1 / rate
    n = int(wintime / timestep) + 1
    dfs = [ df.iloc[i:i+n] for i in range(0, df.shape[0], n) ]

    return dfs


def mk_labels(dfs, theta, wintime):

    # Convert time limit to timedelta object for comparison
    limit = wintime * theta
    limit = datetime.timedelta(seconds=limit)

    labels = []
    plabel = 0
    for df in dfs:

        # Get time differences for the window
        t = df.index.to_series()
        dt = t.diff().fillna(0).cumsum()

        # Split df in theta and 1 - theta fractions if window is big enough
        if dt.iloc[-1] >= limit:

            # Get True for theta fracion, False for 1 - theta
            dt_bool = dt < limit
            fidxs = dt_bool.index[dt_bool == False]
            tidxs = dt_bool.index[dt_bool == True]
            ndf = df.loc[fidxs]
            df = df.loc[tidxs]
        else:
            ndf = None

        # This window: theta fraction
        counts = df[df.notnull()].value_counts()
        l = len(counts)

        # If there are no labels in this window, assign the one from the
        # previous 1 - theta fraction
        if l == 0:
            label = plabel
        elif l >= 1:
            label = int(counts.idxmax())

        labels.append(label)

        # Next window: 1 - theta fraction, if it exists
        # Otherwise set the label to noevent
        if ndf is not None:
            counts = ndf[ndf.notnull()].value_counts()
            l = len(counts)
            if l == 0:
                plabel = 0
            elif l >= 1:
                plabel = int(counts.idxmax())

        else:
            plabel = 0

    labels = np.array(labels).reshape(-1,1)

    return labels


# from https://scipy-cookbook.readthedocs.io/items/KalmanFiltering.html
def kalman(z, Q=1e-4, R=1e-2):

    # intial parameters
    n = z.shape[0]
    sz = (n,)

    # Variance
    # Q = 1e-4

    # Init
    xhatm = np.zeros(sz)     # a priori estimate of x
    Pm = np.zeros(sz)        # a priori error estimate
    xhat = np.zeros(sz)      # a posteriori estimate of x
    P = np.zeros(sz)         # a posteriori error estimate
    K = np.zeros(sz)         # gain or blending factor

    # R = 0.1**2 # estimate of measurement variance, change to see effect

    # intial guesses
    xhat[0] = z[0]
    P[0] = 1.0
    
    for k in range(1, n):
        # time update
        xhatm[k] = xhat[k-1]
        Pm[k] = P[k-1]+Q

        # measurement update
        K[k] = Pm[k]/( Pm[k]+R )
        xhat[k] = xhatm[k]+K[k]*(z[k]-xhatm[k])
        P[k] = (1-K[k])*Pm[k]

    return xhat


def downsampling(data, lbl=0):

    # Get labels
    labels = data[:,-1]

    # Count how many of each type
    u, c = np.unique(labels, return_counts=True)
    counts = dict(zip(u, c))

    # Compute the average of the minority classes
    counts.pop(lbl)
    avg = int(np.mean(list(counts.values())))

    # Extract majority class and resample it
    idxs = labels == lbl
    maj = data[idxs,:]
    maj = resample(maj, replace=False, n_samples=avg)
    mmin = data[~idxs,:]

    # Rejoin
    downsampled = np.r_[ maj, mmin ]

    # Shuffle
    np.random.shuffle(downsampled)

    return downsampled


if __name__ == '__main__':
    pass
